
/***********************************************************************
* Copyright 1998-2020 CERN for the benefit of the EvtGen authors       *
*                                                                      *
* This file is part of EvtGen.                                         *
*                                                                      *
* EvtGen is free software: you can redistribute it and/or modify       *
* it under the terms of the GNU General Public License as published by *
* the Free Software Foundation, either version 3 of the License, or    *
* (at your option) any later version.                                  *
*                                                                      *
* EvtGen is distributed in the hope that it will be useful,            *
* but WITHOUT ANY WARRANTY; without even the implied warranty of       *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
* GNU General Public License for more details.                         *
*                                                                      *
* You should have received a copy of the GNU General Public License    *
* along with EvtGen.  If not, see <https://www.gnu.org/licenses/>.     *
***********************************************************************/

#include "EvtGenModels/EvtKKLambdaC.hh"

#include "EvtGenBase/EvtGenKine.hh"
#include "EvtGenBase/EvtPDL.hh"
#include "EvtGenBase/EvtParticle.hh"
#include "EvtGenBase/EvtReport.hh"
#include "EvtGenBase/EvtSemiLeptonicBaryonAmp.hh"

#include "EvtGenModels/EvtKKLambdaCFF.hh"

#include <string>

std::string EvtKKLambdaC::getName() const
{
    return "KK_LAMBDAC_SL";
}

EvtDecayBase* EvtKKLambdaC::clone() const
{
    return new EvtKKLambdaC;
}

void EvtKKLambdaC::decay( EvtParticle* p )
{
    p->initializePhaseSpace( getNDaug(), getDaugs() );

    m_calcamp->CalcAmp( p, m_amp2, m_ffmodel.get() );
}

void EvtKKLambdaC::initProbMax()
{
    EvtId parnum, mesnum, lnum, nunum;

    parnum = getParentId();
    mesnum = getDaug( 0 );
    lnum = getDaug( 1 );
    nunum = getDaug( 2 );

    //double mymaxprob = m_calcamp->CalcMaxProb(parnum,mesnum,
    //                           lnum,nunum,m_ffmodel);
    double mymaxprob = 1e3;
    setProbMax( mymaxprob );
}

void EvtKKLambdaC::init()
{
    checkNDaug( 3 );

    //We expect the parent to be a dirac
    //and the daughters to be dirac lepton neutrino

    checkSpinParent( EvtSpinType::DIRAC );
    checkSpinDaughter( 0, EvtSpinType::DIRAC );
    checkSpinDaughter( 1, EvtSpinType::DIRAC );
    checkSpinDaughter( 2, EvtSpinType::NEUTRINO );

    m_ffmodel = std::make_unique<EvtKKLambdaCFF>( getNArg(), getArgs() );

    m_calcamp = std::make_unique<EvtSemiLeptonicBaryonAmp>();
}
