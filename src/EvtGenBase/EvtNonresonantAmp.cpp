
/***********************************************************************
* Copyright 1998-2020 CERN for the benefit of the EvtGen authors       *
*                                                                      *
* This file is part of EvtGen.                                         *
*                                                                      *
* EvtGen is free software: you can redistribute it and/or modify       *
* it under the terms of the GNU General Public License as published by *
* the Free Software Foundation, either version 3 of the License, or    *
* (at your option) any later version.                                  *
*                                                                      *
* EvtGen is distributed in the hope that it will be useful,            *
* but WITHOUT ANY WARRANTY; without even the implied warranty of       *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
* GNU General Public License for more details.                         *
*                                                                      *
* You should have received a copy of the GNU General Public License    *
* along with EvtGen.  If not, see <https://www.gnu.org/licenses/>.     *
***********************************************************************/

#include "EvtGenBase/EvtNonresonantAmp.hh"

#include "EvtGenBase/EvtComplex.hh"
#include "EvtGenBase/EvtCyclic3.hh"
#include "EvtGenBase/EvtDalitzCoord.hh"
#include "EvtGenBase/EvtdFunction.hh"

#include <cassert>
#include <cmath>
#include <iostream>

using EvtCyclic3::Index;
using EvtCyclic3::Pair;
using std::endl;

EvtNonresonantAmp::EvtNonresonantAmp( EvtDalitzPlot* dp,
                                      EvtPto3PAmp::NumType type,
                                      EvtCyclic3::Pair pair1, double par1,
                                      EvtCyclic3::Pair pair2, double par2,
                                      EvtSpinType::spintype spin ) :
    EvtAmplitude<EvtDalitzPoint>(),
    m_dalitzSpace{ dp },
    m_type( type ),
    m_pair1( pair1 ),
    m_pair2( pair2 ),
    m_par1( par1 ),
    m_par2( par2 ),
    m_spin( spin )
{
}

EvtComplex EvtNonresonantAmp::amplitude( const EvtDalitzPoint& dalitzPoint ) const
{
    // flat model
    if ( m_type == EvtPto3PAmp::NONRES ) {
        return 1;
    }

    // "linear model" (prop. to m^2)
    else if ( m_type == EvtPto3PAmp::NONRES_LIN ) {
        return dalitzPoint.q( m_pair1 );
    }

    // Chen-Chua-Soni
    else if ( m_type == EvtPto3PAmp::NONRES_CCS ) {
        double s = dalitzPoint.q( m_pair1 );
        double smin = m_dalitzSpace->qAbsMin( m_pair1 );
        return sqrt( s - smin ) / ( s * log( s * m_par1 ) );
    }

    // exp{par*m^2) (Belle model, Garmash et al, PRD71)
    else if ( m_type == EvtPto3PAmp::NONRES_EXP ) {
        return exp( m_par1 * dalitzPoint.q( m_pair1 ) );
    }

    // exp(par1*m12^2 + par2*m13^2) (Belle model, Garmash et al, PRD71)
    else if ( m_type == EvtPto3PAmp::NONRES_EXP_ADD ) {
        return exp( m_par1 * dalitzPoint.q( m_pair1 ) +
                    m_par2 * dalitzPoint.q( m_pair2 ) );
    }

    // Laura model (P.Harrison et al, BAD806)
    else if ( m_type == EvtPto3PAmp::NONRES_LAURA ) {
        double m = sqrt( dalitzPoint.q( m_pair1 ) );
        double mmin = sqrt( m_dalitzSpace->qAbsMin( m_pair1 ) );
        double dm = m - mmin;
        assert( dm > 0 );
        double cosTh = 1;
        int ispin = EvtSpinType::getSpin2( m_spin );
        if ( ispin > 0 ) {
            cosTh = dalitzPoint.cosTh( EvtCyclic3::next( m_pair1 ), m_pair1 );
            if ( ispin > 2 )
                cosTh *= cosTh;
        }
        return pow( dm, m_par1 ) * exp( dm * m_par2 ) * cosTh;
    }

    return 0;
}
