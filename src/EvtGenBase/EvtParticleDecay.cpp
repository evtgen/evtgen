
/***********************************************************************
* Copyright 1998-2020 CERN for the benefit of the EvtGen authors       *
*                                                                      *
* This file is part of EvtGen.                                         *
*                                                                      *
* EvtGen is free software: you can redistribute it and/or modify       *
* it under the terms of the GNU General Public License as published by *
* the Free Software Foundation, either version 3 of the License, or    *
* (at your option) any later version.                                  *
*                                                                      *
* EvtGen is distributed in the hope that it will be useful,            *
* but WITHOUT ANY WARRANTY; without even the implied warranty of       *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
* GNU General Public License for more details.                         *
*                                                                      *
* You should have received a copy of the GNU General Public License    *
* along with EvtGen.  If not, see <https://www.gnu.org/licenses/>.     *
***********************************************************************/

#include "EvtGenBase/EvtParticleDecay.hh"

#include "EvtGenBase/EvtId.hh"
#include "EvtGenBase/EvtPDL.hh"
#include "EvtGenBase/EvtParticle.hh"
#include "EvtGenBase/EvtRandom.hh"
#include "EvtGenBase/EvtReport.hh"

#include <string>
#include <vector>

void EvtParticleDecay::printSummary() const
{
    if ( m_decay != nullptr ) {
        m_decay->printSummary();
    }
}

void EvtParticleDecay::chargeConj( EvtParticleDecay* decay )
{
    m_brfrsum = decay->m_brfrsum;
    m_massmin = decay->m_massmin;

    m_decay = decay->m_decay->clone();

    int ndaug = decay->m_decay->getNDaug();
    int narg = decay->m_decay->getNArg();
    double brfr = decay->m_decay->getBranchingFraction();
    std::string name = decay->m_decay->getName();
    EvtId ipar = EvtPDL::chargeConj( decay->m_decay->getParentId() );
    int i;
    EvtId* daug = new EvtId[ndaug];
    for ( i = 0; i < ndaug; i++ ) {
        daug[i] = EvtPDL::chargeConj( decay->m_decay->getDaug( i ) );
    }
    //Had to add 1 to make sure the vector is not empty!
    std::vector<std::string> args;
    for ( i = 0; i < narg; i++ ) {
        args.push_back( decay->m_decay->getArgStr( i ) );
    }

    m_decay->saveDecayInfo( ipar, ndaug, daug, narg, args, name, brfr );

    if ( decay->m_decay->getFSR() ) {
        m_decay->setFSR();
    }

    delete[] daug;
}
