#!/bin/bash

########################################################################
# Copyright 1998-2024 CERN for the benefit of the EvtGen authors       #
#                                                                      #
# This file is part of EvtGen.                                         #
#                                                                      #
# EvtGen is free software: you can redistribute it and/or modify       #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# (at your option) any later version.                                  #
#                                                                      #
# EvtGen is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public License    #
# along with EvtGen.  If not, see <https://www.gnu.org/licenses/>.     #
########################################################################

# This script installs EvtGen with all external dependencies.
# The variable INSTALL_PREFIX specifies the installation location.
# The variable VERSION specifies the tag of EvtGen you want to use.
# The list of available tags can be found by either going to the url
# https://phab.hepforge.org/source/evtgen/tags/master
# or issuing the command (without the need to clone the git repository)
# git ls-remote --tags http://phab.hepforge.org/source/evtgen.git | cut -d '/' -f3
# The recommended versions of the external dependencies are given below.
# Later versions should be OK as well, assuming their C++ interfaces do not change.
# HepMC (either HepMC2 or HepMC3, the latter is recommended) is mandatory.
# Whether to use a particular external dependency can be specified by setting
# the corresponding USEXXX variable to "ON" or "OFF".
# Note that some earlier EvtGen versions will not be compatible with all
# external dependency versions given below, owing to C++ interface differences;
# see the specific tagged version of the EvtGen/README file for guidance.
# It is also not possible to compile Tauola++ on macOS at present, unless you
# are building on a volume with a case sensitive file system, so this is
# disabled by default.
# Similarly, we have encountered problems building Sherpa on macOS, so that it
# also disabled by default. NB Sherpa is anyway not used by EvtGen versions < 3.
# To obtain this script use the "Download File" option on the right of the webpage:
# https://phab.hepforge.org/source/evtgen/browse/master/setupEvtGen.sh?view=raw

# Location in which to install
INSTALL_PREFIX="/usr/local"

# EvtGen version or tag number (or branch name)
VERSION="R03-00-00-beta1"

# EvtGen git URL
# HepForge main repo
EVTGEN_URL="https://phab.hepforge.org/source/evtgen.git"
# CERN GitLab mirror
#EVTGEN_URL="https://gitlab.cern.ch/evtgen/evtgen.git"

# Options to pass to 'make', e.g. "-j 6"
BUILDARGS=""

# HepMC version numbers - change HEPMCMAJORVERSION to 2 in order to use HepMC2
HEPMCMAJORVERSION="3"
HEPMC2VER="2.06.11"
HEPMC3VER="3.3.0"
HEPMC2PKG="HepMC-$HEPMC2VER"
HEPMC3PKG="HepMC3-$HEPMC3VER"
HEPMC2TAR="hepmc$HEPMC2VER.tgz"
HEPMC3TAR="$HEPMC3PKG.tar.gz"
HEPMCBASEURL="https://hepmc.web.cern.ch/hepmc/releases"
if [ "$HEPMCMAJORVERSION" -lt "3" ]
then
    USEHEPMC3="OFF"
    HEPMCURL=$HEPMCBASEURL/$HEPMC2TAR
    HEPMCTAR=$HEPMC2TAR
else
    USEHEPMC3="ON"
    HEPMCURL=$HEPMCBASEURL/$HEPMC3TAR
    HEPMCTAR=$HEPMC3TAR
fi

# Pythia version number with no decimal points, e.g. 8310 corresponds to version 8.310
# This follows the naming convention of Pythia install tar files
USEPYTHIA="ON"
PYTHIAVER="8312"
PYTHIAPKG="pythia$PYTHIAVER"
PYTHIATAR="$PYTHIAPKG.tgz"
PYTHIABASEURL="https://pythia.org/download"
PYTHIAURL="$PYTHIABASEURL/pythia${PYTHIAVER:0:2}/$PYTHIATAR"

# Photos++ version number
USEPHOTOS="ON"
PHOTOSVER="3.64"
PHOTOSPKG="PHOTOS"
PHOTOSDIR="PHOTOS.$PHOTOSVER"
PHOTOSTAR="$PHOTOSDIR.tar.gz"
PHOTOSBASEURL="https://photospp.web.cern.ch/resources"
PHOTOSURL="$PHOTOSBASEURL/$PHOTOSDIR/$PHOTOSTAR"

# Tauola++ version number
USETAUOLA="ON"
TAUOLAVER="1.1.8"
TAUOLAPKG="TAUOLA"
TAUOLADIR="TAUOLA.$TAUOLAVER"
TAUOLATAR="$TAUOLADIR.tar.gz"
TAUOLABASEURL="https://tauolapp.web.cern.ch/resources"
TAUOLAURL="$TAUOLABASEURL/$TAUOLADIR/$TAUOLATAR"

# Sherpa version number
USESHERPA="ON"
SHERPAMAJORVERSION="3"
if [ "$SHERPAMAJORVERSION" -lt "3" ]
then
    USESHERPA3="OFF"
    SHERPAVER="2.2.16"
else
    USESHERPA3="ON"
    SHERPAVER="3.0.0"
fi
SHERPAPKG="sherpa-v$SHERPAVER"
SHERPATAR="$SHERPAPKG.tar.gz"
SHERPABASEURL="https://gitlab.com/sherpa-team/sherpa/-/archive"
SHERPAURL="$SHERPABASEURL/v$SHERPAVER/$SHERPATAR"

# Determine OS
osArch=`uname`

# macOS settings
if [ "$osArch" == "Darwin" ]
then
    export LANG=en_US.UTF-8
    export LC_ALL=en_US.UTF-8
    # need to disable Tauola
    USETAUOLA="OFF"
    USESHERPA="OFF"
fi

#This is for systems with cmake and cmake3
if command -v cmake3; then
    CMAKE=cmake3
else
    CMAKE=cmake
fi

echo Will install EvtGen version $VERSION and its dependencies in $INSTALL_PREFIX

# Make sure that the dependencies are picked up from our local install
delpath()
{
    eval "$1=\$(echo \$$1 | sed -e s%^$2\$%% -e s%^$2\:%% -e s%:$2\:%:%g -e s%:$2\\\$%%)"
}
addpath_front()
{
    delpath $*
    eval "$1=$2:\$$1"
}
addpath_front CMAKE_PREFIX_PATH $INSTALL_PREFIX
export CMAKE_PREFIX_PATH

BUILD_BASE=`mktemp -d` || exit 1

echo Temporary build area is $BUILD_BASE

cd $BUILD_BASE

mkdir -p tarfiles
mkdir -p sources
mkdir -p builds

echo Downloading EvtGen source from GIT, using URL: $EVTGEN_URL

cd sources
git clone $EVTGEN_URL evtgen
cd evtgen
git checkout $VERSION

echo Downloading sources of external dependencies

cd $BUILD_BASE/tarfiles

echo Downloading HepMC source from: $HEPMCURL
curl -O $HEPMCURL
if [ $USEPYTHIA == "ON" ]
then
    echo Downloading Pythia8 source from: $PYTHIAURL
    curl -O $PYTHIAURL
fi
if [ $USEPHOTOS == "ON" ]
then
    echo Downloading Photos source from: $PHOTOSURL
    curl -O $PHOTOSURL
fi
if [ $USETAUOLA == "ON" ]
then
    echo Downloading Tauola source from: $TAUOLAURL
    curl -O $TAUOLAURL
fi
if [ $USESHERPA == "ON" ]
then
    echo Downloading Sherpa source from: $SHERPAURL
    curl -O $SHERPAURL
fi

cd $BUILD_BASE/sources

echo Extracting external dependencies
tar -xzf $BUILD_BASE/tarfiles/$HEPMCTAR
if [ $USEPYTHIA == "ON" ]
then
    tar -xzf $BUILD_BASE/tarfiles/$PYTHIATAR
fi
if [ $USEPHOTOS == "ON" ]
then
    tar -xzf $BUILD_BASE/tarfiles/$PHOTOSTAR
fi
if [ $USETAUOLA == "ON" ]
then
    tar -xzf $BUILD_BASE/tarfiles/$TAUOLATAR
fi
if [ $USESHERPA == "ON" ]
then
    tar -xzf $BUILD_BASE/tarfiles/$SHERPATAR
fi

if [ "$osArch" == "Darwin" ]
then
    # Patch PHOTOS and TAUOLA on Darwin (macOS)
    if [ $USEPHOTOS == "ON" ]
    then
        sed -i '' 's/soname/install_name/g' PHOTOS/Makefile
        patch -p0 < $BUILD_BASE/sources/evtgen/platform/photos_Darwin.patch
    fi
    if [ $USETAUOLA == "ON" ]
    then
        sed -i '' 's/soname/install_name/g' TAUOLA/Makefile
        patch -p0 < $BUILD_BASE/sources/evtgen/platform/tauola_Darwin.patch
    fi

    # Uncomment the lines below to force usage of Apple clang
    # export CC=clang
    # export CXX=clang++
    # sed -i '' 's/\-lstdc++/-lc++/g' PHOTOS/platform/make.inc.in
    # sed -i '' 's/\-lstdc++/-lc++/g' TAUOLA/platform/make.inc.in
fi

cd $BUILD_BASE

if [ "$HEPMCMAJORVERSION" -lt "3" ]
then
    # HepMC2 install

    echo Installing HepMC from $BUILD_BASE/sources/$HEPMC2PKG
    mkdir -p $BUILD_BASE/builds/HepMC2
    cd $BUILD_BASE/builds/HepMC2
    $CMAKE -DCMAKE_INSTALL_PREFIX:PATH=$INSTALL_PREFIX $BUILD_BASE/sources/$HEPMC2PKG -Dmomentum:STRING=GEV -Dlength:STRING=MM
    make $BUILDARGS
    make install

    if [ $USEPYTHIA == "ON" ]
    then
        echo Installing Pythia8 from $BUILD_BASE/sources/$PYTHIAPKG
        cd $BUILD_BASE/sources/$PYTHIAPKG
        ./configure --enable-shared --prefix=$INSTALL_PREFIX
        make $BUILDARGS
        make install
    fi

    if [ $USEPHOTOS == "ON" ]
    then
        echo Installing PHOTOS from $BUILD_BASE/sources/$PHOTOSPKG
        cd $BUILD_BASE/sources/$PHOTOSPKG
        ./configure --with-hepmc3= --with-hepmc=$INSTALL_PREFIX --prefix=$INSTALL_PREFIX
        make $BUILDARGS
        make install
    fi

    if [ $USETAUOLA == "ON" ]
    then
        echo Installing TAUOLA from $BUILD_BASE/sources/$TAUOLAPKG
        cd $BUILD_BASE/sources/$TAUOLAPKG
        ./configure --without-hepmc3 --with-hepmc=$INSTALL_PREFIX --prefix=$INSTALL_PREFIX
        make $BUILDARGS
        make install
    fi

else
    # HepMC3 install

    echo Installing HepMC3 from $BUILD_BASE/sources/$HEPMC3PKG
    mkdir -p $BUILD_BASE/builds/HepMC3
    cd $BUILD_BASE/builds/HepMC3
    $CMAKE -DHEPMC3_ENABLE_ROOTIO:BOOL=OFF -DHEPMC3_ENABLE_PYTHON:BOOL=OFF -DCMAKE_INSTALL_PREFIX:PATH=$INSTALL_PREFIX $BUILD_BASE/sources/$HEPMC3PKG
    make $BUILDARGS
    make install

    if [ $USEPYTHIA == "ON" ]
    then
        echo Installing Pythia8 from $BUILD_BASE/souces/$PYTHIAPKG
        cd $BUILD_BASE/sources/$PYTHIAPKG
        ./configure --enable-shared --prefix=$INSTALL_PREFIX
        make $BUILDARGS
        make install
    fi

    if [ $USEPHOTOS == "ON" ]
    then
        echo Installing PHOTOS from $BUILD_BASE/sources/$PHOTOSPKG
        cd $BUILD_BASE/sources/$PHOTOSPKG
        ./configure --without-hepmc --with-hepmc3=$INSTALL_PREFIX --prefix=$INSTALL_PREFIX
        make $BUILDARGS
        make install
    fi

    if [ $USETAUOLA == "ON" ]
    then
        echo Installing TAUOLA from $BUILD_BASE/sources/$TAUOLAPKG
        cd $BUILD_BASE/sources/$TAUOLAPKG
        ./configure --without-hepmc --with-hepmc3=$INSTALL_PREFIX --prefix=$INSTALL_PREFIX
        make $BUILDARGS
        make install
    fi
fi

if [ $USESHERPA == "ON" ]
then
    echo Installing Sherpa $SHERPAMAJORVERSION from $BUILD_BASE/sources/$SHERPAPKG
    if [ "$SHERPAMAJORVERSION" -lt "3" ]
    then
        cd $BUILD_BASE/sources/$SHERPAPKG
        autoreconf -i
        ./configure --with-sqlite3=install --prefix=$INSTALL_PREFIX
        make $BUILDARGS
        make install
    else
        mkdir -p $BUILD_BASE/builds/Sherpa
        cd $BUILD_BASE/builds/Sherpa
        $CMAKE -DCMAKE_INSTALL_PREFIX:PATH=$INSTALL_PREFIX $BUILD_BASE/sources/$SHERPAPKG
        $CMAKE --build .
        $CMAKE --install .
    fi
fi

echo Installing EvtGen from $BUILD_BASE/sources/evtgen
mkdir -p $BUILD_BASE/builds/evtgen
cd $BUILD_BASE/builds/evtgen
$CMAKE -DCMAKE_INSTALL_PREFIX:PATH=$INSTALL_PREFIX \
       -DEVTGEN_HEPMC3:BOOL=$USEHEPMC3 -DHEPMC${HEPMCMAJORVERSION}_ROOT_DIR:PATH=$INSTALL_PREFIX \
       -DEVTGEN_PYTHIA:BOOL=$USEPYTHIA -DPYTHIA8_ROOT_DIR:PATH=$INSTALL_PREFIX \
       -DEVTGEN_PHOTOS:BOOL=$USEPHOTOS -DPHOTOSPP_ROOT_DIR:PATH=$INSTALL_PREFIX \
       -DEVTGEN_TAUOLA:BOOL=$USETAUOLA -DTAUOLAPP_ROOT_DIR:PATH=$INSTALL_PREFIX \
       -DEVTGEN_SHERPA:BOOL=$USESHERPA -DSHERPA_ROOT_DIR:PATH=$INSTALL_PREFIX \
       -DEVTGEN_SHERPA3:BOOL=$USESHERPA3 \
       $BUILD_BASE/sources/evtgen
make $BUILDARGS
make install

echo Setup done.
echo To complete, set the Pythia8 data path:
if [ "$PYTHIAVER" -lt "8200" ]
then
    echo PYTHIA8DATA=$INSTALL_PREFIX/xmldoc
else
    echo PYTHIA8DATA=$INSTALL_PREFIX/share/Pythia8/xmldoc
fi

echo If installation fully successful you can remove the temporary build area $BUILD_BASE
cd $BUILD_BASE
