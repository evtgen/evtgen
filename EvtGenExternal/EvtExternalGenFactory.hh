
/***********************************************************************
* Copyright 1998-2020 CERN for the benefit of the EvtGen authors       *
*                                                                      *
* This file is part of EvtGen.                                         *
*                                                                      *
* EvtGen is free software: you can redistribute it and/or modify       *
* it under the terms of the GNU General Public License as published by *
* the Free Software Foundation, either version 3 of the License, or    *
* (at your option) any later version.                                  *
*                                                                      *
* EvtGen is distributed in the hope that it will be useful,            *
* but WITHOUT ANY WARRANTY; without even the implied warranty of       *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
* GNU General Public License for more details.                         *
*                                                                      *
* You should have received a copy of the GNU General Public License    *
* along with EvtGen.  If not, see <https://www.gnu.org/licenses/>.     *
***********************************************************************/

#ifndef EVTEXTERNALGENFACTORY_HH
#define EVTEXTERNALGENFACTORY_HH

#include "EvtGenModels/EvtAbsExternalGen.hh"

#include <map>
#include <memory>

// Description: A factory type method to create engines for external physics
// generators like Pythia.

class EvtExternalGenFactory final {
  public:
    enum class GenId
    {
        PythiaGenId = 0,
        TauolaGenId
    };

    static EvtExternalGenFactory& getInstance();

    EvtAbsExternalGen* getGenerator( const GenId genId );

    void initialiseAllGenerators();

    void definePythiaGenerator( std::string xmlDir, bool convertPhysCodes,
                                bool useEvtGenRandom = true );
    void defineTauolaGenerator( bool useEvtGenRandom = true,
                                bool seedTauolaFortran = true,
                                bool useTauolaRadiation = false,
                                double infraredCutOffTauola = 1.0e-7 );

    //methods to add configuration commands to the pythia generators
    //void addPythiaCommand( std::string generator, std::string module, std::string param, std::string value);
    //void addPythia6Command(std::string generator, std::string module, std::string param, std::string value);

  private:
    EvtExternalGenFactory() = default;
    ~EvtExternalGenFactory() = default;
    EvtExternalGenFactory( const EvtExternalGenFactory& ) = delete;
    EvtExternalGenFactory( EvtExternalGenFactory&& ) = delete;
    EvtExternalGenFactory& operator=( const EvtExternalGenFactory& ) = delete;
    EvtExternalGenFactory& operator=( EvtExternalGenFactory&& ) = delete;

    typedef std::map<GenId, std::unique_ptr<EvtAbsExternalGen>> ExtGenMap;
    //typedef std::map<GenId, std::map<std::string, std::vector<std::string>>> ExtGenCommandMap;

    ExtGenMap m_extGenMap;
    //ExtGenCommandMap m_extGenCommandMap;
};

#endif
